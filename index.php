


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        Chiến dịch "Con Mơ Điều Giản Dị": We Are Afamily 2016
    </title>
    <link rel="shortcut icon" href="/Style/skin/logo-af.ico" type="image/x-icon" />
    <meta name="author" content="blogtamsu.vn" /><meta name="copyright" content="Copyright © 2016 by blogtamsu.vn" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta id="metaDescription" name="description" content="blogtamsu.vn với mong muốn truyền cảm hứng và lan tỏa tình yêu thương, các giá trị trong gia đình" />
    <meta id="metaKeywords" name="keywords" />
    <meta id="metaNewKeywords" name="news_keywords" />
    <meta id="fbTitle" property="og:title" content="Chiến dịch &quot;Con Mơ Điều Giản Dị&quot;: We Are Afamily 2016" />
    <meta id="fbDescription" property="og:description" content="&quot;We Are Family&quot; là một chiến dịch thường niên của blogtamsu.vn, với mong muốn truyền cảm hứng và lan tỏa tình yêu thương, các giá trị trong gia đình" />
    <meta id="fbImage" property="og:image" content="http://afamily1.vcmedia.vn/web_images/avatarsharthumb.jpg" />
    <meta id="fbUrl" property="og:url" />
    <meta id="fbSiteName" property="og:site_name" content="blogtamsu.vn" />
    <meta id="fbType" property="og:type" content="article" />
    <meta property="fb:app_id" content="1129210607097827" />
    <meta name="abstract" content="We Are Family 2016: Chiến dịch mùa gia đình yêu thương" />
    <meta name="distribution" content="Global" />
    <meta http-equiv="refresh" content="1200" />
    <meta name="Revisit-After" content="1 Days" />
    <meta name="Rating" content="General" />
    <meta name="robots" content="index,follow" />
    <meta name="Googlebot" content="index,follow,archive" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="css/reset.css" rel="stylesheet" type="text/css" />
    <link href="css/base.css" rel="stylesheet" type="text/css" />
    <link href="css/custom.css" rel="stylesheet" type="text/css" />
    <link href="css/reponsivelandingpage.css" rel="stylesheet" type="text/css" />
    <link href="css/animate.css" rel="stylesheet" type="text/css" />
    <link href="css/swiper.min.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-3.0.0.min.js"></script>
    <script src="js/jquery.easings.min.js"></script>
    <script src="js/scrolloverflow.min.js"></script>
    <script src="js/swiper.jquery.min.js"></script>
    <script src="js/jquery.fullPage.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/wow.min.js"></script>

    <script>
        $(document).ready(function () {
            new WOW().init();
            wafldp.init();
        });
    </script>
    <script>
        var wafldp = {
            fullPageCreated: false,
            specialWrp: '.special-wrp',
            specialContent: '.special-content',
            wafBg: '.waflp-bg',
            wafBgMobile: '.waflp-bg-mobile',
            clickPage: '.bgwitharrow',
            videoContent: '.wafc-vcontent',
            special: '.special',
            init: function () {
                var root = this;
                root.createpage();
                root.changestyle();
                $(window).resize(function () {
                    root.changestyle();
                });
            },
            changestyle: function () {
                var wbody = $(window).width();
                $(wafldp.wafBgMobile).css('height', $(window).height());
                if (wbody > 1000) {
                    $(wafldp.specialWrp).css('padding-top', ($(window).height() - $(wafldp.specialContent).height()) / 2);
                    $(wafldp.specialWrp).css('height', ($(window).height() - ($(window).height() - $(wafldp.specialContent).height()) / 2));
                    $(wafldp.wafBg).css('height', $(window).height());
                    wafldp.createpage();
                    $(wafldp.clickPage).click(function (e) {
                        e.preventDefault();
                        $.fn.fullpage.moveTo(2);
                    });
                } else {
                    wafldp.fullPageCreated = false;
                    $.fn.fullpage.destroy('all');
                    $(wafldp.wafBg).css('height', $(wafldp.videoContent).height() + $(wafldp.special).height() + 100);
                    $(wafldp.specialWrp).css('padding-top', 0);
                    $(wafldp.specialWrp).css('height', 'auto');
                    $(wafldp.clickPage).click(function (e) {
                        e.preventDefault();
                        $('html,body').animate({
                            scrollTop: $('.sc-text').offset().top - 10
                        }, 500);
                    });
                }
            },
            createpage: function () {
                if (wafldp.fullPageCreated === false) {
                    wafldp.fullPageCreated = true;
                    $('#fullpage').fullpage({
                        css3: true,
                        scrollingSpeed: 1000
                    });
                }
            }
        };
    </script>
</head>
<body>
<form method="post" action="" id="form1">
    <div class="aspNetHidden">
        <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKLTcwNjUzNzgzN2Rk8qLtFt/rcpEsQxHoMxlLzTLbBNHD2Af7iB9201vFuD0=" />
    </div>

    <div class="aspNetHidden">

        <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C50F42C6" />
    </div>
    <div class="waf-landingpage" id="fullpage">
        <div class="waflp-bg-mobile" style="background-image: url('images/bg-landingpage-mobile.jpg')">

            <div class="toppage">
                <a href="/" class="logo-waf"></a>
                <div class="sharefb">
                    <a href="javascript:;" onclick="shareClick('http://blogtamsu.vn/');" class="icsharefb"></a>
                </div>
            </div>
                <span class="waflp-slogan">Chiến dịch
                    <br />
                    mùa gia đình yêu thương<br />
                    của Afamily.vn<br />
                </span>
                <span class="wafc-text">
                    <img src="images/cmdg.png"></span>
            <span href="javascript:;" class="date-start">bắt đầu từ ngày 05/07/2016</span>
        </div>
        <div class="waflp-bg section" style="background-image: url('images/bg-landingpage.jpg')">


            <a href="http://blogtamsu.vn/" class="logo-af" target="_blank"></a>
            <div class="toppage">
                <a href="javascript:void()" class="logo-waf"></a>
                <div class="sharefb">
                    <a href="javascript:;" onclick="shareClick('http://blogtamsu.vn/');" class="icsharefb"></a>
                </div>
            </div>
            <div class="special">
                <a href="#avatar" class="bgwitharrow wow fadeInUp"></a>
            </div>
            <div class="waflp-content">
                    <span class="waflp-slogan">Chiến dịch
                        <br />
                        mùa gia đình yêu thương<br />
                        của Afamily.vn<br />
                    </span>
                    <span class="wafc-text">
                        <img src="images/cmdg.png"></span>
                <span href="javascript:;" class="date-start">bắt đầu từ ngày 05/07/2016</span>

                <div class="wafc-vcontent">
                    <div class="video-wrp">
                        <ul class="wafcv-list clearfix swiper-wrapper">

                            <li class="wafvli clearfix swiper-slide">
                                <div class="wafv-wrapper clearfix">
                                    <div class="wappervideo">
                                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/XM20OMdOE24" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                    <div class="wafv-info clearfix">
                                        <div class="left-clm fl">
                                            <h3 class="wafv-title"><a href="/">Lần đầu tôi nói yêu bố mẹ</a></h3>
                                            <span class="wafv-sapo">Khi câu nói "Con yêu bố mẹ..." được cất lên, đầu dây bên kia lặng đi trong vài giây còn ở bên này, đôi mắt nhà sư trẻ cũng nhòe đi vì xúc động.</span>
                                        </div>
                                        <div class="right-clm fr">

                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="wafvli clearfix swiper-slide">
                                <div class="wafv-wrapper clearfix">
                                    <div class="wappervideo">
                                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/jocMtLlEldY" frameborder="0" allowfullscreen=""></iframe>
                                    </div>
                                    <div class="wafv-info clearfix">
                                        <div class="left-clm fl">
                                            <h3 class="wafv-title"><a href="javascript:;">Tôi muốn được lắng nghe </a></h3>
                                            <span class="wafv-sapo">Bạn có một câu chuyện để kể. Bạn mong muốn được lắng nghe. Chúng tôi ở đây và sẵn lòng lắng nghe câu chuyện của bạn</span>
                                        </div>
                                        <div class="right-clm fr">

                                        </div>
                                    </div>
                                </div>
                            </li>


                        </ul>
                    </div>
                    <div class="swiper-pagination"></div>
                    <script>
                        var slidedbl = new Swiper('.video-wrp', {
                            slidesPerView: 'auto',
                            pagination: '.wafc-vcontent .swiper-pagination',
                            spaceBetween: 20,
                            slidesPerGroup: 2,
                            simulateTouch: true,
                            speed: 600,
                            hashNav: true,
                            paginationClickable: true,
                            effect: 'fade'
                        });

                    </script>
                </div>

                <a href="https://docs.google.com/forms/d/1uVN9fCGmk41GeW3rpOrSU09LIUxsmZBb7mmVTJHo9sI/viewform"
                   target="_blank" rel="nofollow" class="register-story">Đăng kí chia sẻ câu chuyện của bạn về gia đình</a>
            </div>
        </div>
        <div class="special-wrp section" id="avatar">
            <div class="special-content clearfix">
                <p class="sc-text">
                    Thay ảnh avatar gia đình
                    <br />
                    và khiến người bạn yêu thương bất ngờ ngay hôm nay
                </p>
                <ul class="sc-avatar">
                    <li class="scali type1">
                        <div class="avatar wcagbcat-ava-frame" id="wcagbcat-ava-frame">
                            <span style="background-image: url('images/loader.gif')" class="loading-upload" id="loading-upload"></span>
                            <img src="images/example-avatar.png" id="wcagbcataf-userava" />
                            <img src="images/bg-cym.png" class="watermark" />
                        </div>
                        <div class="wcagbcat-ava-frame avatar" id="img-out" style="display: none;">
                            <img id="testImg" />
                        </div>
                        <div class="btngroup clearfix">

                            <a href="javascript:;" class="btnshare" id="btn-fb-login">Dùng avatar facebook</a>
                            <input type="file" name="filedata" id="uploadavatar" class="ctruploadAvatar" />
                            <a href="javascript:;" class="btnupload btnupload1" onclick="LoginFbAndUpload()">Tải ảnh lên</a>
                            <a href="javascript:;" id="taivemay" class="btnsave" style="display: none">Lưu</a>

                        </div>
                    </li>
                    <li class="scali type2">
                        <div class="avatar wcagbcat-ava-frame2" id="wafagbcat-ava-frame2">
                            <span style="background-image: url('images/loader.gif')" class="loading-upload" id="loading-upload2"></span>
                            <img src="images/example-avatar.png" id="wafagbcataf-userava2" />
                            <img src="images/bg-cycn.png" class="watermark" />
                        </div>
                        <div class="wcagbcat-ava-frame2 avatar" id="img-out2" style="display: none;">
                            <img id="testImg2" />
                        </div>
                        <div class="btngroup clearfix">
                            <a href="javascript:;" class="btnshare" id="btn-fb-login2">Dùng avatar facebook</a>
                            <input type="file" name="filedata2" id="uploadGdm" class="ctruploadAvatar" />
                            <a href="javascript:;" class="btnupload btnupload2">Tải ảnh lên</a>
                            <a href="javascript:;" id="taivemaygdm" class="btnsave" style="display: none">Lưu</a>
                        </div>
                    </li>
                    <li class="scali type3">
                        <div class="avatar wcagbcat-ava-frame3" id="wafagbcat-ava-frame3">
                            <span style="background-image: url('images/loader.gif')" class="loading-upload" id="loading-upload3"></span>
                            <img src="images/example-avatar.png" id="wafagbcataf-userava3" />
                            <img src="images/bg-cyb.png" class="watermark" />
                        </div>
                        <div class="wcagbcat-ava-frame3 avatar" id="img-out3" style="display: none;">
                            <img id="testImg3" />
                        </div>
                        <div class="btngroup clearfix">
                            <a href="javascript:;" class="btnshare" id="btn-fb-login3">Dùng avatar facebook</a>
                            <input type="file" name="filedata3" id="uploadCyb" class="ctruploadAvatar" />
                            <a href="javascript:;" class="btnupload btnupload3">Tải ảnh lên</a>
                            <a href="javascript:;" id="taivemaycyb" class="btnsave" style="display: none">Lưu</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <script>
        var checkUpload = 0;
        // html5 upload
        var maxsize = 0;
        maxsize = 10 * 1048576; /*10Mb*/
        //upload CON YÊU MẸ
        document.getElementById('uploadavatar').addEventListener('change', function () {
            $('#loading-upload').show();
            var flag = "cym";
            var file = this.files[0];
            var reader = new FileReader();
            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    var thumbSize = 500;
                    var canvas = document.getElementById("tempCanvas");

                    var img = new Image();
                    img.onload = function (e) {

                        var w = img.width;
                        var h = img.height;

                        canvas.width = w;
                        canvas.height = h;
                        var c = canvas.getContext("2d");

                        if (w != h) {

                            var sourceX = 0;
                            var sourceY = 0;
                            var sourceWidth = 500;
                            var sourceHeight = 500;
                            var destWidth = sourceWidth;
                            var destHeight = sourceHeight;
                            var destX = canvas.width / 2 - destWidth / 2;
                            var destY = canvas.height / 2 - destHeight / 2;
                            if (w < h) {
                                var newH = 500 * h / w;
                                sourceY = (newH - 500) / 2;
                                c.drawImage(this, 0, 0, 500, newH);
                            } else {
                                var newW = 500 * w / h;
                                sourceX = (newW - 500) / 2;
                                c.drawImage(this, 0, 0, newW, 500);
                            }

                            var img2 = new Image();
                            img2.onload = function (e) {
                                c.clearRect(0, 0, canvas.width, canvas.height);
                                canvas.width = 500;
                                canvas.height = 500;
                                c.drawImage(img2, sourceX, sourceY, sourceWidth, sourceHeight, 0, 0, destWidth, destHeight);
                                callbackUploadImage(canvas.toDataURL("image/png"), flag);
                            };
                            img2.src = canvas.toDataURL("image/png");

                        } else {
                            c.drawImage(img, 0, 0, w, h);
                            callbackUploadImage(canvas.toDataURL("image/png"), flag);
                        }
                    };
                    img.src = e.target.result;

                };
            })(file);
            // Read in the image file as a data URL.
            reader.readAsDataURL(file);

            return;

            //}, { scope: 'email, publish_actions' });
        }, false);

        //upload GIA ĐÌNH MÌNH
        document.getElementById('uploadGdm').addEventListener('change', function () {
            $('#loading-upload2').show();
            var flag = "gdm";
            var file = this.files[0];
            var reader = new FileReader();
            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    var thumbSize = 500;
                    var canvas = document.getElementById("tempCanvas");

                    var img = new Image();
                    img.onload = function (e) {

                        var w = img.width;
                        var h = img.height;

                        canvas.width = w;
                        canvas.height = h;
                        var c = canvas.getContext("2d");


                        if (w != h) {

                            var sourceX = 0;
                            var sourceY = 0;
                            var sourceWidth = 500;
                            var sourceHeight = 500;
                            var destWidth = sourceWidth;
                            var destHeight = sourceHeight;
                            var destX = canvas.width / 2 - destWidth / 2;
                            var destY = canvas.height / 2 - destHeight / 2;
                            if (w < h) {
                                var newH = 500 * h / w;
                                sourceY = (newH - 500) / 2;
                                c.drawImage(this, 0, 0, 500, newH);
                            } else {
                                var newW = 500 * w / h;
                                sourceX = (newW - 500) / 2;
                                c.drawImage(this, 0, 0, newW, 500);
                            }

                            var img2 = new Image();
                            img2.onload = function (e) {
                                c.clearRect(0, 0, canvas.width, canvas.height);
                                canvas.width = 500;
                                canvas.height = 500;
                                c.drawImage(img2, sourceX, sourceY, sourceWidth, sourceHeight, 0, 0, destWidth, destHeight);
                                callbackUploadImage(canvas.toDataURL("image/png"), flag);
                            };
                            img2.src = canvas.toDataURL("image/png");

                        } else {
                            c.drawImage(img, 0, 0, w, h);
                            callbackUploadImage(canvas.toDataURL("image/png"), flag);
                        }
                    };
                    img.src = e.target.result;
                };
            })(file);
            // Read in the image file as a data URL.
            reader.readAsDataURL(file);

            return;

        }, false);

        //upload COM YÊU BA
        document.getElementById('uploadCyb').addEventListener('change', function () {
            $('#loading-upload3').show();
            var flag = "cyb";
            var file = this.files[0];
            var reader = new FileReader();
            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    var thumbSize = 500;
                    var canvas = document.getElementById("tempCanvas");

                    var img = new Image();
                    img.onload = function (e) {

                        var w = img.width;
                        var h = img.height;

                        canvas.width = w;
                        canvas.height = h;
                        var c = canvas.getContext("2d");


                        if (w != h) {

                            var sourceX = 0;
                            var sourceY = 0;
                            var sourceWidth = 500;
                            var sourceHeight = 500;
                            var destWidth = sourceWidth;
                            var destHeight = sourceHeight;
                            var destX = canvas.width / 2 - destWidth / 2;
                            var destY = canvas.height / 2 - destHeight / 2;
                            if (w < h) {
                                var newH = 500 * h / w;
                                sourceY = (newH - 500) / 2;
                                c.drawImage(this, 0, 0, 500, newH);
                            } else {
                                var newW = 500 * w / h;
                                sourceX = (newW - 500) / 2;
                                c.drawImage(this, 0, 0, newW, 500);
                            }

                            var img2 = new Image();
                            img2.onload = function (e) {
                                c.clearRect(0, 0, canvas.width, canvas.height);
                                canvas.width = 500;
                                canvas.height = 500;
                                c.drawImage(img2, sourceX, sourceY, sourceWidth, sourceHeight, 0, 0, destWidth, destHeight);
                                callbackUploadImage(canvas.toDataURL("image/png"), flag);
                            };
                            img2.src = canvas.toDataURL("image/png");

                        } else {
                            c.drawImage(img, 0, 0, w, h);
                            callbackUploadImage(canvas.toDataURL("image/png"), flag);
                        }
                    };
                    img.src = e.target.result;
                };
            })(file);
            // Read in the image file as a data URL.
            reader.readAsDataURL(file);

            return;
            var rFilter = /^(image\/JPG|image\/jpg|image\/jpeg|image\/png|image\/JPEG|image\/PNG)$/i;
            if (!rFilter.test(file.type)) {
                alert('File ' + file.name + ' sai định dạng .jpg, .jpeg, .png');
                return false;
            }

            if (IsUnicode(file.name)) {
                alert("Tên file " + file.name + " không hợp lệ. Tên file hợp lệ là tên chỉ chứa các chữ cái a-z, 0-9, '-', không dấu và không có ký tự đặc biệt.");
                return false;
            }
            /*//little test for filesize*/
            if (file.size > maxsize) {
                alert('File ' + file.name + ' có dung lượng file lớn hơn ' + (maxsize / 1048576) + 'Mb. Vui lòng resize trước khi upload.');
                return false;
            }

            //uploadFile(file, flag);
        }, false);


        function IsUnicode(str) {
            var pattern = /[^\u0000-\u0080]+/;
            return pattern.test(str);
        }


        function callbackUploadImage(base64Img, flag) {
            //convertImgToDataURLviaCanvas(link, function (base64Img) {
            if (flag == "cym") {
                $('#wcagbcataf-userava').attr('src', base64Img);
                init(base64Img, flag);
                $('.btnupload1').hide();
                $("#taivemay").attr("download", "blogtamsu.vn-conyeume.jpg");
                setTimeout(function() {
                    $('#loading-upload').hide();
                    $("#taivemay").show();
                }, 2000);

            } else if (flag == "gdm") {
                $('#wafagbcataf-userava2').attr('src', base64Img);
                init(base64Img, flag);
                $('.btnupload2').hide();
                $("#taivemaygdm").attr("download", "blogtamsu.vn-congiadinh.jpg");
                setTimeout(function () {
                    $('#loading-upload2').hide();
                    $("#taivemaygdm").show();
                }, 2000);
            } else {
                $('#wafagbcataf-userava3').attr('src', base64Img);
                init(base64Img, flag);
                $('.btnupload3').hide();
                $("#taivemaycyb").attr("download", "blogtamsu.vn-conyeuba.jpg");
                setTimeout(function () {
                    $('#loading-upload3').hide();
                    $("#taivemaycyb").show();
                }, 2000);
            }
            //callbackGenAvatar();
            // });
        }
        $('.wcagbcat-ava-frame').css('height', $('.wcagbcat-ava-frame').width() + 'px');

    </script>
    <canvas id="layer1" style="z-index: 1; position: absolute; left: 0px; top: 0px; display: none;" height="500" width="500"></canvas>
    <canvas id="layer2" style="z-index: 2; position: absolute; left: 0px; top: 0px; display: none;" height="500" width="500"></canvas>
    <canvas id="layer3" style="z-index: 3; position: absolute; left: 0px; top: 0px; display: none;" height="500" width="500"></canvas>


    <script type="text/javascript">
        var layer1;
        var layer2;
        var layer3;
        var ctx1;
        var ctx2;
        var ctx3;
        var x = 500;
        var y = 500;
        var dx = 2;
        var dy = 4;
        var WIDTH = 500;
        var HEIGHT = 500;
        var city = new Image();
        var mask = new Image();


        function init(src64, flag) {
            //console.log(src64);
            city.src = src64;
            city.style.width = WIDTH;
            city.style.height = HEIGHT;
            $(city).attr('style', 'webkit-filter: grayscale(1);-webkit-filter: grayscale(100%);filter: grayscale(100%);filter: url(#greyscale);filter: gray;');

            var imagebase = "";
            if (flag == "cym")
                imagebase = "images/Landing-ava1_v2.png";
            else if (flag == "gdm")
                imagebase = "images/Landing-ava3_v2.png";
            else
                imagebase = "images/Landing-ava2_v2.png";
            mask.src = imagebase;
            mask.style.width = WIDTH;
            mask.style.height = HEIGHT;

            layer1 = document.getElementById("layer1");
            ctx1 = layer1.getContext("2d");
            layer2 = document.getElementById("layer2");
            ctx2 = layer2.getContext("2d");
            layer3 = document.getElementById("layer3");
            ctx3 = layer3.getContext("2d");
            //drawAll();

            var drawInterval = setInterval(drawAll, 50);


            if (flag == "cym") {
                setTimeout(function () {
                    clearInterval(drawInterval);
                    document.getElementById('testImg').src = document.getElementById("layer3").toDataURL("image/jpeg", 1.0);
                    document.getElementById('taivemay').href = document.getElementById("layer3").toDataURL("image/jpeg", 1.0);

                }, 2000);
            } else if (flag == "gdm") {
                setTimeout(function () {
                    clearInterval(drawInterval);
                    document.getElementById('testImg2').src = document.getElementById("layer3").toDataURL("image/jpeg", 1.0);
                    document.getElementById('taivemaygdm').href = document.getElementById("layer3").toDataURL("image/jpeg", 1.0);

                }, 2000);
            } else {
                setTimeout(function () {
                    clearInterval(drawInterval);
                    document.getElementById('testImg3').src = document.getElementById("layer3").toDataURL("image/jpeg", 1.0);
                    document.getElementById('taivemaycyb').href = document.getElementById("layer3").toDataURL("image/jpeg", 1.0);

                }, 2000);
            }


        }

        function drawAll() {
            draw1();
            draw2();
            draw3();
        }

        function draw1() {
            ctx1.clearRect(0, 0, WIDTH, HEIGHT);
            ctx1.drawImage(city, 0, 0);
        }

        function draw2() {
            ctx2.clearRect(0, 0, WIDTH, HEIGHT);
            ctx2.drawImage(mask, 0, 0);
        }

        function draw3() {
            ctx3.drawImage(layer1, 0, 0);
            ctx3.drawImage(layer2, 0, 0);
        }


    </script>
    <script type="text/javascript">

        function convertImgToDataURLviaCanvas(url, callback, outputFormat) {
            var img = new Image();
            img.style.width = 500;
            img.style.height = 500;
            img.crossOrigin = 'Anonymous';
            img.onload = function () {
                var canvas = document.createElement('CANVAS');
                var ctx = canvas.getContext('2d');
                var dataURL;
                canvas.height = 500;
                canvas.width = 500;
                ctx.drawImage(this, 0, 0);
                dataURL = canvas.toDataURL(outputFormat);
                callback(dataURL);
                canvas = null;
            };
            img.src = url;
        }

        $(document).ready(function () {
            $('#btn-fb-login').on('click', function () {
            });
        });

        function canvasImage() {
            html2canvas($("#wcagbcat-ava-frame"), {
                onrendered: function (canvas) {
                    theCanvas = canvas;
                    document.body.appendChild(canvas);
                    $("#img-out").append(canvas);
                }
            });
        }


    </script>
    <script>

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5&appId=1129210607097827";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));


        window.fbAsyncInit = function () {
            FB.init({
                appId: '1129210607097827',
                status: true,
                xfbml: true,
                version: 'v2.3'
            });
            FB.Event.subscribe('auth.statusChange', function (response) {
                if (response.authResponse) {
                    // user has auth'd your app and is logged into Facebook

                } else {

                }
            });
        };


        $('#btn-fb-login').on('click', function () {
            FB.login(function (data) {

                $('#loading-upload').show();
                FB.api('/me', function (me) {
                    console.log(me);
                    if (me.name) {
                        var ava = 'http://graph.facebook.com/' + me.id + '/picture?type=large&width=400&height=400';
                        console.log(ava);
                        //$('#wcagbcataf-userava').attr('src', ava);

                        convertImgToDataURLviaCanvas(ava, function (base64Img) {
                            //console.log(base64Img);
                            $('#wcagbcataf-userava').attr('src', base64Img);
                            init(base64Img);
                            //callbackGenAvatar();
                            callbackUploadImage(base64Img, "cym");
                        });

                    }
                });
            }, { scope: 'email, publish_actions' });
        });
        $('#btn-fb-login2').on('click', function () {
            FB.login(function (data) {

                $('#loading-upload2').show();
                FB.api('/me', function (me) {
                    console.log(me);
                    if (me.name) {
                        var ava = 'http://graph.facebook.com/' + me.id + '/picture?type=large&width=400&height=400';
                        console.log(ava);
                        //$('#wcagbcataf-userava').attr('src', ava);

                        convertImgToDataURLviaCanvas(ava, function (base64Img) {
                            //console.log(base64Img);
                            $('#wcagbcataf-userava').attr('src', base64Img);
                            init(base64Img);
                            //callbackGenAvatar();
                            callbackUploadImage(base64Img, "gdm");
                        });

                    }
                });
            }, { scope: 'email, publish_actions' });
        });
        $('#btn-fb-login3').on('click', function () {
            FB.login(function (data) {

                $('#loading-upload3').show();
                FB.api('/me', function (me) {
                    console.log(me);
                    if (me.name) {
                        var ava = 'http://graph.facebook.com/' + me.id + '/picture?type=large&width=400&height=400';
                        console.log(ava);
                        //$('#wcagbcataf-userava').attr('src', ava);

                        convertImgToDataURLviaCanvas(ava, function (base64Img) {
                            //console.log(base64Img);
                            $('#wcagbcataf-userava').attr('src', base64Img);
                            init(base64Img);
                            //callbackGenAvatar();
                            callbackUploadImage(base64Img, "cyb");
                        });

                    }
                });
            }, { scope: 'email, publish_actions' });
        });

        //function callbackGenAvatar() {
        //    $('.btnupload1').css('display', 'none');
        //    $('#img-out').css('display', 'block');
        //    $('#liGetFromFb').css('display', 'none');
        //    $('#getFromLocal').css('display', 'none');
        //    $('#downloadImage').css('display', 'block');
        //    $('#shareImage').css('display', 'block');
        //    $('#loading-upload').hide();
        //}


        function shareClick(hr) {
            var wleft = screen.width / 2 - 700 / 2;
            var wtop = screen.height / 2 - 450 / 2;

            var link = hr != null ? hr : document.location.href;

            var w = window.open("http://www.facebook.com/sharer.php?u=" + link, "chia sẻ",
                'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=700, height=485, top=' + wtop + ', left=' + wleft
            );

            w.focus();
            return false;
        }

    </script>
    <canvas id="tempCanvas" style="display: none;"></canvas>
</form>
</body>
</html>
